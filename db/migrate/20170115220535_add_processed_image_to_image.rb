class AddProcessedImageToImage < ActiveRecord::Migration[5.0]
  def change
    add_column :images, :processed_image, :string
    add_column :images, :processed_file_url, :string
  end
end
