class AddNameAndFileToImage < ActiveRecord::Migration[5.0]
  def up
    add_attachment :images, :file
    add_column :images, :name, :string, index: true
  end

  def down
    remove_attachemnt :images, :file
    remove_column :images, :name
  end
end
