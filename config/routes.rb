Rails.application.routes.draw do

  resources :images, except: [:index]
  get '/', to: 'images#index', as: :root

  post '/images/:id/process', to: 'images#process_image', as: :process_image

end
