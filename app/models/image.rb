class Image < ApplicationRecord

  has_attached_file :file, styles: { default: '300x300#' }
  validates_attachment :file, presence: true, content_type: { content_type: /\Aimage\/.*\z/ }

  validates :name, presence: true

  before_destroy :delete_processed_image

  def process

    kernel = [
      [ 0.5 , 0.75 , 0.5 ],
      [ 0.75 , 1 , 0.75 ],
      [ 0.5 , 0.75 , 0.5 ],
    ]

    # kernel = [
    #   [ -1 , -1 , -1 ],
    #   [ -1 , 9 , -1 ],
    #   [ -1 , -1 , -1 ],
    # ]

    img_path = file.path(:default)
    img = Magick::Image.read(img_path).first

    # Получаем пиксели изображения
    pixels = img.get_pixels(0,0,img.columns,img.rows)

    # Генерируем путь и картинку "До"
    post_image_path = get_processed_file_path img_path, "_post"
    post_img = img

    # Генерируем картинку и путь "После"
    processed_image_path = get_processed_file_path img_path
    # Увеличиваем размер изображеиня, для добавления "ложной" рамки
    processed_img = img.resize(img.columns + kernel.first.count - 1 , img.rows + kernel.count - 1)

    # Создаём изображение в рамке
    processed_img.store_pixels(0,0, processed_img.columns, processed_img.rows, get_fake_pixels( kernel , img , pixels )) 
    
    pocessed_pixels = processed_img.get_pixels(0,0, processed_img.columns , processed_img.rows)
    # Применяем фильтр к изображению в рамке
    pixels  = apply_filter( kernel , processed_img , pocessed_pixels )

    # Получаем изображение без рамки
    processed_img = processed_img.resize( img.columns , img.rows )
    processed_img.store_pixels(0,0, processed_img.columns, processed_img.rows, pixels) 

    # Созраняем изображение
    processed_img.write( processed_image_path )

    # Задаём аттрибуты текущего объекта (Изображения)
    self.processed_image = processed_image_path
    self.processed_file_url = get_processed_file_url file.url(:default)

    self.save
  end

  protected 

    def get_fake_pixels ( kernel , img , pixels)
      half_kernel_width  = kernel.first.count / 2
      half_kernel_height = kernel.count / 2

      img_width = img.columns
      img_height = img.rows

      top_pixels = []

      (half_kernel_height+1).downto(2) do | i |
        #  get the top side pixels
        needle_row = pixels[((i-1)*img_width)..(img_width*i-1)]
        #  get the lef-top corner pixels
        top_pixels += needle_row[1..(half_kernel_width)].reverse
        # put the top side pixels
        top_pixels += needle_row
        # get the right-top corner pixels
        top_pixels += needle_row[(-half_kernel_width-1)..(-2)].reverse
      end

      bottom_pixels = []

      (-2).downto(-half_kernel_height-1) do | i |
        #  get the bottom side pixels
        needle_row = pixels[(img_width*i)..(img_width*(i+1)-1)]
        #  get the left-bottom corner pixels
        bottom_pixels += needle_row[1..(half_kernel_width)].reverse
        # put the bottom side pixels
        bottom_pixels += needle_row
        # get the right-botom pixels
        bottom_pixels += needle_row[(-half_kernel_width-1)..(-2)].reverse
      end


      # inserting left and right side pixels
      for i in 0..(img_height-1) do
        pixels_row = pixels[(i*(img_width + half_kernel_width*2))..(i*(img_width + half_kernel_width*2)+img_width-1)]

        left_inserted_pixels  = pixels_row[1..half_kernel_width].reverse
        
        right_inserted_pixels = pixels_row[(-half_kernel_width-1)..(-2)].reverse
        
        pixels.insert(i*(img_width + half_kernel_width*2), *left_inserted_pixels)
        pixels.insert(i*(img_width + half_kernel_width*2)+half_kernel_width+img_width, *right_inserted_pixels)
      end

      pixels = top_pixels + pixels
      pixels = pixels + bottom_pixels
    end

    def apply_filter ( kernel , img , pixels )
      img_width = img.columns
      img_height = img.rows

      half_kernel_width  = kernel.first.count / 2
      half_kernel_height = kernel.count / 2

      output_pixels = []
      pixel = nil


      for y in half_kernel_height..(img_height - 1 - half_kernel_height) do
        for x in half_kernel_width..(img_width - 1 - half_kernel_width) do
            rSum = gSum = bSum = kSum = 0

            for j in 0..(half_kernel_height*2) do
              for i in 0..(half_kernel_width*2) do
                pixel_pos_x = x + i - half_kernel_width
                pixel_pos_y = y + j - half_kernel_height

                pixel = pixels[ pixel_pos_y * img_width + pixel_pos_x ]

                kernel_val = kernel[i][j]

                rSum += kernel_val * pixel.red
                gSum += kernel_val * pixel.green
                bSum += kernel_val * pixel.blue

                kSum += kernel_val
              end
            end
              pixel = pixels[ y * img_width + x ]

            kSum = 1 if kSum <= 0

            rSum /= kSum 
            rSum = 0 if rSum < 0
            rSum = 65535 if rSum > 65535

            gSum /= kSum 
            gSum = 0 if gSum < 0
            gSum = 65535 if gSum > 65535

            bSum /= kSum 
            bSum = 0 if bSum < 0
            bSum = 65535 if bSum > 65535

            pixel.red   = rSum
            pixel.green = gSum
            pixel.blue  = bSum

            output_pixels << pixel 
          end
        end
      return output_pixels
    end

    def get_processed_file_path ( img_path , prefix = "_processed" )
      img_folder = img_path.split('/')
      img_folder.delete img_folder.last
      img_folder = img_folder.join('/') + "/"

      processed_file_name = img_path.split('/').last.split('.')
      processed_file_name[0] += prefix
      processed_file_name = processed_file_name.join('.')

      processed_file_path = img_folder + processed_file_name 
    end

    def get_processed_file_url ( img_url, prefix = "_processed" )
      img_folder = img_url.split('/')
      img_folder.delete img_folder.last
      img_folder = img_folder.join('/') + "/"

      processed_file_name = img_url.split('/').last.split('.')
      processed_file_name[0] += prefix
      processed_file_name = processed_file_name.join('.')

      processed_file_path = img_folder + processed_file_name 
    end

    def delete_processed_image
      File.delete(processed_image) if !processed_image.nil? && File.exist?(processed_image)
    end

end
