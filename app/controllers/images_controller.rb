class ImagesController < ApplicationController

  def index
    @images = Image.all
  end 

  def new
    @image = Image.new
  end

  def create
    @image = Image.new(image_params)
    if @image.save
      flash['success'] = 'Изображение успешно создано'
      redirect_to action: :index
    else
      render :new
    end
  end

  def edit
    @image = Image.find_by(id: params[:id])
    unless @image
      flash[:dnager] = "Изображение не наидено"
      redirect_to action: :index
    end
  end

  def update
    @image = Image.find_by(id: params[:id])
    if @image.update_attributes(image_params)
      redirect_to action: :index
    else
      render :edit
    end
  end

  def destroy
    @image = Image.find_by(id: params[:id])
    if @image.destroy
      flash[:success] = "Изображение успешно удалено"
    else
      flash[:danger] = "Изображение не наидено"
    end
    redirect_to action: :index
  end

  def process_image
    image = Image.find_by(id: params[:id])
    if image
      image.process
      flash[:success] = "Изображение успешно обработано"
      redirect_to request.referer
    else
      redirect_to request.referer
      flash[:danger] = "Изображение не наидено"
    end
  end

  private

    def image_params
      params.require(:image).permit(:name, :file)
    end

end
