# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

get_image_preview = () ->
  
  if $('#image_form').length
    file_input = $('input#image_file[type="file"]')

    file_input.on 'change', (e) ->
      input = e.target

      if input.files && input.files[0]

        image_container = $('#image_file_pic_container')
        image = image_container.children('img')
        image_container.show() if image_container.is ':hidden'

        reader = new FileReader()

        reader.onload = (e) ->
          image.attr('src', e.target.result)

        reader.readAsDataURL input.files[0]

fade_with_processed_iamge = () ->
  $('a.with_fade').hover () ->
      console.log $(this)
      $(this).find('img:last').fadeToggle()

$(document).on 'turbolinks:load', () ->
  get_image_preview()
  fade_with_processed_iamge()